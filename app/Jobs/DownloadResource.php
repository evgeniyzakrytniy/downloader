<?php

namespace App\Jobs;

use App\Resource;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class DownloadResource implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 3;
    public $resource_url;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($resource_url)
    {
        $this->resource_url = $resource_url;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $dir = storage_path() . "/files/";
        $file = file_get_contents($this->resource_url);
        $filename = time();

        if ($file) {

            is_dir($dir) || @mkdir($dir) || die("Can't Create folder");
            copy($this->resource_url, $dir . DIRECTORY_SEPARATOR . $filename);

        } 

        $resource = new Resource;
        $resource->filename = $filename;
        $resource->resource_url = $this->resource_url;
        $resource->save();
        
    }
}
