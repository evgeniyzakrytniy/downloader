<?php

namespace App\Http\Controllers;

use App\Job;
use App\FailedJob;
use App\Resource;

use Illuminate\Http\Request;

class ResourcesController extends Controller
{
    public function index() {

		$resources = Resource::all();
		$jobs = Job::all();
		$failed_jobs = FailedJob::all();

		foreach ($jobs as $job) {

			$payload = json_decode( $job->payload );
			$command = unserialize( $payload->data->command );
		    $job->resource_url = $command->resource_url;
		}

		foreach ($failed_jobs as $failed_job) {

			$payload = json_decode( $failed_job->payload );
			$command = unserialize( $payload->data->command );
		    $failed_job->resource_url = $command->resource_url;
		}

		return view('index', 
            compact(
            	'jobs',
            	'failed_jobs',
                'resources'
            )
        );

	}

	public function download($filename) {

		$file_path = storage_path() . "/files/" . $filename; 

		if ( file_exists( $file_path ) ) { 

			return \Response::download( $file_path, $filename ); 
		 
		} 
		
	}
}
