<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DownloadResource extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'resource:download {url}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Download resource by url';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $filename = time();

        \App\Jobs\DownloadResource::withChain([
            new \App\Jobs\StoreResourceData($filename)
        ])->dispatch($this->argument('url'), $filename);

    }
}
