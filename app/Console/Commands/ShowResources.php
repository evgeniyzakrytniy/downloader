<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ShowResources extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'resource:list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Show all downloaded resources';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $jobs = \App\Job::all();

        foreach ($jobs as $job) {

            $payload = json_decode( $job->payload );
            $command = unserialize( $payload->data->command );
            echo ($command->resource_url) . "\n";

        }
    }
}
