
	{!! Form::open(['route' => 'download_resource', 'method' => 'post']) !!}

		{!! Form::label('resource', 'Resource:') !!}
		{!! Form::text('resource', null, ['class' => 'form-control']) !!}

		{!! Form::submit('Download TO Laravel Storage', ['class' => 'btn btn-success form-control']) !!}

	{!! Form::close() !!}
	
	<br><br>
	<h2>Downloaded Resource</h2>
	
	<table class="table">
		@if (!$resources->isEmpty())
			<thead>
				<th>Filename</th>
				<th>Resource</th>
			</thead>
		@endif

		<tbody>
			@forelse($resources as $resource)
			    <tr>
					<td>{{ $resource->filename }} </td>
					<td>{{ $resource->resource_url }} </td>
					<td>
						<a href="{{ route('download', ['filename' => $resource->filename ]) }}">Download FROM Laravel Storage</a>
					</td>
				</tr>
			@empty
			    <p>No data yet</p>
			@endforelse
		</tbody>
	</table>	

	<br><br>
	<h2>Actual Queue</h2>
	
	<table class="table">
		@if (!$jobs->isEmpty())
			<thead>
				<th>Resource</th>
			</thead>
		@endif

		<tbody>
			@forelse($jobs as $job)
			    <tr>
					<td>{{ $job->resource_url }}</td>
				</tr>
			@empty
			    <p>No data yet</p>
			@endforelse
		</tbody>
	</table>

	<h2>Failed Jobs</h2>

	<table class="table">
		@if (!$failed_jobs->isEmpty())
			<thead>
				<th>Resource</th>
				<th>Exception</th>
			</thead>
		@endif

		<tbody>
			@forelse($failed_jobs as $failed_job)
			    <tr>
					<td>{{ $failed_job->resource_url }}</td>
					<td>{{ str_limit($failed_job->exception, 150, '...') }}</td>
				</tr>
			@empty
			    <p>No data yet</p>
			@endforelse
		</tbody>
	</table>