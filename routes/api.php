<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('resources', function() { 
    return App\Resource::all();
});

Route::post('resources', function(Request $request) {

    App\Jobs\DownloadResource::dispatch($request->resource);
    return redirect()->route('resources');

})->name('download_resource');;